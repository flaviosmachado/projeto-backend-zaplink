'use strict';

const Hapi = require('@hapi/hapi');

const mongoose = require('mongoose');

const mongoURL = "mongodb+srv://qaninja:qaninja@cluster0.hgkcnrg.mongodb.net/zaplinkdb?retryWrites=true&w=majority"

mongoose.connect(mongoURL, {useNewUrlParser: true}, {useUnifiedTopolgy: true})

mongoose.connection.on('connected', ()=>{
    console.log('MongoDB connected');
})

mongoose.connection.on('error', (error)=>{
    console.log('MongoDB nao ok' + error);
})

const contactRoutes = require('./routes/contact.routes')

const server = Hapi.server({
    port: 3000,
    host: 'localhost',
    routes: {
        cors: {
            origin: ['*']
        }
    }
});

server.route(contactRoutes)

server.start((err)=> {
    if(err) {
        throw err;
    }
    console.log('Server running on %s', server.info.uri);
});


process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

exports.init = async ()=> {
    return server;
}
